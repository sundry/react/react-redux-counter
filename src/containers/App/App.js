import React, { Component } from 'react';
import './app.css';
import Counters from '../Counters/Counters';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Counters />
      </div>
    );
  }
}
