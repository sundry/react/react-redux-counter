import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Counter from '../../components/Counter/Counter';
import { connect } from 'react-redux';
import * as CountersActions from '../../redux/actions/CountersActions';
import { bindActionCreators } from 'redux';

class Counters extends Component {
  onIncrement(index) {
    this.props.actions.Increment(index);
  }
  onDecrement(index) {
    this.props.actions.Decrement(index);
  }
  addCounter() {
    this.props.actions.AddCounter();
  }
  removeCounter(index) {
    this.props.actions.RemoveCounter(index);
  }
  render() {
    return (
      <div>
        {this.props.counters.map((value, index) => {
          return (
            <Counter
              key={index}
              value={value}
              onIncrement={() => this.onIncrement(index)}
              onDecrement={() => this.onDecrement(index)}
              removeCounter={() => this.removeCounter(index)}
            />
          );
        })}
        <hr />
        <button onClick={this.addCounter.bind(this)}>Add Counter</button>
      </div>
    );
  }
}

Counters.propTypes = {
  counters: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  counters: state.CountersReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(CountersActions, dispatch),
  };
}

const CountersContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Counters);

export default CountersContainer;
