import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Counters from '../Counters';
import { Provider } from 'react-redux';
import store from '../../../redux/store';

Enzyme.configure({ adapter: new Adapter() });

function setup() {
  const wrapper = mount(
    <Provider store={store}>
      <Counters />
    </Provider>
  );
  return {
    wrapper,
  };
}

describe('Counters Container', () => {
  it('should check length of h1', () => {
    const { wrapper } = setup();
    expect(wrapper.find('h1').length).toEqual(1);
  });
  it('should check text of h1', () => {
    const { wrapper } = setup();
    expect(wrapper.find('h1').text()).toEqual('0');
  });
  it('should click increment counter', () => {
    const { wrapper } = setup();
    const incrementBtn = wrapper
      .find('div')
      .children()
      .find('div')
      .children()
      .find('button')
      .at(0);
    incrementBtn.simulate('click');
    expect(wrapper.find('h1').text()).toEqual('1');
  });
  it('should click decrement counter', () => {
    const { wrapper } = setup();
    const decrementBtn = wrapper
      .find('div')
      .children()
      .find('div')
      .children()
      .find('button')
      .at(1);
    decrementBtn.simulate('click');
    expect(wrapper.find('h1').text()).toEqual('0');
  });
  it('should click add counter', () => {
    const { wrapper } = setup();
    const addCounterBtn = wrapper.find('button').at(3);
    addCounterBtn.simulate('click');
    expect(wrapper.find('h1').length).toEqual(2);
  });
  it('should click remove counter', () => {
    const { wrapper } = setup();
    const removeBtn = wrapper
      .find('div')
      .children()
      .find('div')
      .children()
      .find('button')
      .at(2);
    removeBtn.simulate('click');
    expect(wrapper.find('h1').length).toEqual(1);
  });
});
