import * as types from '../constants/ActionTypes';

export function Increment(index) {
  return dispatch => {
    dispatch({
      type: types.INCREMENT,
      index,
    });
  };
}

export function Decrement(index) {
  return dispatch => {
    dispatch({
      type: types.DECREMENT,
      index,
    });
  };
}

export function AddCounter() {
  return dispatch => {
    dispatch({
      type: types.ADD_COUNTER,
    });
  };
}

export function RemoveCounter(index) {
  return dispatch => {
    dispatch({
      type: types.REMOVE_COUNTER,
      index,
    });
  };
}
