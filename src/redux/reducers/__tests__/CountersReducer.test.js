import CountersReducer from '../CountersReducer';
import * as types from '../../constants/ActionTypes';
import deepFreeze from 'deep-freeze';

describe('Counters Reducer', () => {
  it('should return the initial state', () => {
    expect(CountersReducer(undefined, {})).toEqual([0]);
  });
  it('should increment the counter', () => {
    const beforeState = [0];
    const afterState = [1];
    const action = { type: types.INCREMENT, index: 0 };
    deepFreeze(beforeState);
    expect(CountersReducer(beforeState, action)).toEqual(afterState);
  });
  it('should decrement the counter', () => {
    const beforeState = [0];
    const afterState = [-1];
    const action = { type: types.DECREMENT, index: 0 };
    deepFreeze(beforeState);
    expect(CountersReducer(beforeState, action)).toEqual(afterState);
  });
  it('should add a counter', () => {
    const beforeState = [0];
    const afterState = [0, 0];
    const action = { type: types.ADD_COUNTER };
    deepFreeze(beforeState);
    expect(CountersReducer(beforeState, action)).toEqual(afterState);
  });
  it('should remove a counter', () => {
    const beforeState = [0, 0];
    const afterState = [0];
    const index = 0;
    const action = { type: types.REMOVE_COUNTER, index };
    deepFreeze(beforeState);
    expect(CountersReducer(beforeState, action)).toEqual(afterState);
  });
});
